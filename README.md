# Validador

Herramienta web para validar documentos que cumplen con la [Política de
Formatos Oficiales de los Documentos Electrónicos Firmados Digitalmente](
https://www.mifirmadigital.go.cr/?smd_process_download=1&download_id=372
) de Costa Rica.

Este proyecto se encuentra en modo mantenimiento. Solo recibirá actualizaciones
que no sean de nuevas características, ya que el código fuente se combinará con
el de un nuevo firmador y validador en un único ejecutable polivalente.
Más información en https://fran.cr/cambios-a-futuro-para-el-proyecto-validador/
y en https://codeberg.org/firmador/firmador donde se continuará su desarrollo.


## Compilación

Se requiere Maven y GraalVM CE. Se ha probado en entornos GNU/Linux y macOS
por ahora.

- Descargar GraalVM CE para la plataforma soportada de su sistema operativo:

https://github.com/graalvm/graalvm-ce-builds/releases/latest

- Descomprimir GraalVM CE con el comando:

    tar xf graalvm-community-*.tar.gz

- Abrir una terminal y ubicarse en el directorio de descargas, cuyo nombre
  puede variar dependiendo del idioma y tipo de sistema. Por ejemplo:

    cd Descargas

- Clonar este repositorio con git:

    git clone https://gitlab.com/firmador/validador.git

- Para que la variable de entorno JAVA_HOME utilice GraalVM CE, ejecutar:

    . env.sh

Tras ello, compilar (puede tardar varios minutos):

    mvn clean package

Generará el ejecutable `validador` en el directorio `target` y unas librerías.


## Prueba del validador por línea de comandos

El uso propuesto es por interfaz web pero se puede generar el reporte XML así:

    cd target
    ./validador DocumentoParaValidar.pdf


## Despliegue web de referencia

La herramienta es libre y puede instalarse en servidores web de manera
independiente. Existe una instalación vinculada con el desarrollo de este
proyecto como despliegue de referencia en el sitio web
https://validador.libre.cr.

Para utilizar el validador desde su propia instalación se debe copiar el
contenido de la carpeta `validador.libre.cr` del repositorio. El servidor debe
contar con PHP, salvo que se genere una implementación que invoque al
validador como en el ejemplo PHP y maneje el reporte XML por su cuenta.
En el caso del código PHP debe encontrar el ejecutable validador generado en
la carpeta `target` tras compilarse y sus librerías dinámicas en la carpeta.
Copiar el validador y estas librerías en la carpeta `validar` y revisar que
el nombre del ejecutable corresponda con el indicado en el fichero index.php
que existe en esa misma carpeta, ya que si se compilara para Windows podría
llamarse `validador.exe` en lugar de `validador`.


## Información para desarrolladores del validador

GraalVM es un compilador AOT y no conoce toda la información que requiere en
tiempo de compilación, por lo que debe lanzarse el validador en varias pruebas
validando todo tipo de documentos con variaciones en contenido. Por ejemplo,
imágenes JPEG en los PDF, documentos ODF, documentos p7m, y documentos en
formatos diferentes a los soportados que no se hayan probado y que ejecuten
rutas con llamadas que dependen de reflection y que pueden hacer caer la
aplicación. GraalVM proporciona un agente que genera metadatos para que en la
próxima compilación no se caiga al ya conocer cómo debe generarse el código.

Se documenta el procedimiento a continuación:

Debe estar configurado para utilizar el compilador Java de GraalVM CE primero:

    . env.sh

Para ejecutar el agente que detecta información adicional de reflection,
generar el JAR tradicional en lugar del ejecutable nativo. Este JAR ejecutable
también se puede utilizar de forma habitual pero la ejecución es mucho más
lenta iniciando, por lo que si se opta por no usar imagen nativa entonces se
sugiere que se desarrolle un validador como servicio que quede en ejecución,
que también consume más memoria:

    mvn -DskipNativeBuild -DskipShade=false clean package

Una vez compilado, ejecutar y realizar operaciones lanzando el JAR con el
agente:

    java -agentlib:native-image-agent=config-merge-dir=src/main/resources/META-INF/native-image/ -jar target/validador.jar /RUTA/A/FICHEROAVALIDAR.pdf

Esto actualizará de ser necesario los metadatos existentes. Se agradecen
contribuciones en las actualizaciones de los metadatos que haya encontrado
para ayudar a mejorar la estabilidad de la aplicación, puede hacerlo mediante
un merge request a este repositorio git.
