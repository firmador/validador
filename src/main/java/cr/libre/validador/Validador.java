/* Validador is a program to validate documents using AdES standards.

Copyright (C) Validador authors.

This file is part of Validador.

Validador is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Validador is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Validador.  If not, see <http://www.gnu.org/licenses/>.  */

package cr.libre.validador;

import eu.europa.esig.dss.diagnostic.SignatureWrapper;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.service.crl.OnlineCRLSource;
import eu.europa.esig.dss.service.ocsp.OnlineOCSPSource;
import eu.europa.esig.dss.spi.DSSUtils;
import eu.europa.esig.dss.spi.x509.CommonTrustedCertificateSource;
import eu.europa.esig.dss.spi.validation.CertificateVerifier;
import eu.europa.esig.dss.spi.validation.CommonCertificateVerifier;
import eu.europa.esig.dss.validation.SignedDocumentValidator;
import eu.europa.esig.dss.validation.reports.Reports;

public class Validador {
    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            System.err.println("No se ha proporcionado un fichero.");
            System.exit(1);
        }
        CommonTrustedCertificateSource trustedCertSource = new CommonTrustedCertificateSource();
        trustedCertSource.addCertificate(DSSUtils.loadCertificate(Thread.currentThread().getContextClassLoader().getResourceAsStream("certs/CA RAIZ NACIONAL - COSTA RICA v2.crt")));
        trustedCertSource.addCertificate(DSSUtils.loadCertificate(Thread.currentThread().getContextClassLoader().getResourceAsStream("certs/CA RAIZ NACIONAL COSTA RICA.cer")));
        CertificateVerifier cv = new CommonCertificateVerifier();
        cv.setOcspSource(new OnlineOCSPSource());
        cv.setCrlSource(new OnlineCRLSource());
        cv.setTrustedCertSources(trustedCertSource);
        DSSDocument document = new FileDocument(args[0]);
        try {
            SignedDocumentValidator documentValidator = SignedDocumentValidator.fromDocument(document);
            documentValidator.setCertificateVerifier(cv);
            Reports reports = documentValidator.validateDocument();
            System.out.println(reports.getXmlSimpleReport());
            for (SignatureWrapper wrapper : reports.getDiagnosticData().getSignatures()) if (!wrapper.getPdfAnnotationChanges().isEmpty()) System.exit(188);
        } catch (UnsupportedOperationException e) {
            System.err.println("Formato no reconocido o soportado");
            System.exit(187);
        }
    }
}
