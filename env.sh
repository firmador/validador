#!/bin/sh
# Usage: . env.sh
DOWNLOAD="$(xdg-user-dir DOWNLOAD)"
if [ -d "$DOWNLOAD" ]
then
  JAVA_HOME="$(ls -d "$DOWNLOAD"/graalvm-community-openjdk-*)"
fi
export JAVA_HOME
export PATH="$JAVA_HOME"/bin:"$PATH"
