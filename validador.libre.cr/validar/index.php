<!doctype html>
<html lang=es-cr>
<meta charset=utf-8>
<meta name=viewport content="width=device-width,initial-scale=1">
<link rel=stylesheet href=../validador.css>
<title>Resultado del validador</title>
<p class=noprint><a href=..>Regresar al validador</a></p>
<?php
set_time_limit(300);
if (!$_FILES) exit("<p>El formulario está vacío, seleccione un documento en el validador.</p>\n");
foreach ($_FILES['documento']['error'] as $actual => $error) {
	if ($error === UPLOAD_ERR_OK) {
		print "<hr>\n";
		print '<h1>' . htmlspecialchars(basename($_FILES['documento']['name'][$actual])) . "</h1>\n";
		$proc = proc_open('./validador ' . $_FILES['documento']['tmp_name'][$actual], [['pipe', 'r'], ['pipe', 'w'], ['pipe', 'a']], $pipes);
		$errors = htmlspecialchars(stream_get_contents($pipes[2]));
		$source = stream_get_contents($pipes[1]);
		fclose($pipes[0]);
		fclose($pipes[1]);
		fclose($pipes[2]);
		$exit_code = proc_close($proc);
		if ($exit_code === 187) {
			print("<p>No se reconoció el formato del fichero o el formato no está soportado.</p>\n");
			continue;
		}
		if ($exit_code === 188) print "<p><strong>Advertencia:</strong> el documento incluye modificaciones (anotaciones) añadidas después de haberse firmado.</p>\n<p>&nbsp;</p>\n";
		// El reporte viene en formato XML, no en HTML, debido a limitaciones con XSLT en GraalVM. Se transforma por ahora desde PHP.
		$report_xml = new DOMDocument();
		if ($source) $report_xml->loadXML($source);
		else {
			print "<p>Ocurrió un error inesperado al validar.</p>\n<pre>\n$errors</pre>\n";
			continue;
		}
		libxml_use_internal_errors(true);
		$report_xsl = new DomDocument();
		$report_xsl->load('simple-report.xslt');
		$xslt = new XSLTProcessor;
		$xslt->importStyleSheet($report_xsl);
		unset($report_xsl);
		libxml_use_internal_errors(false); // Nasty well known PHP DOMDocument memory leak workaround
		libxml_use_internal_errors(true);
		print $xslt->transformToXML($report_xml);
		unset($report_xml);
		libxml_use_internal_errors(false);
		libxml_use_internal_errors(true);
		unset($xslt);
		libxml_use_internal_errors(false);
		if ($errors) print "<!--Código de salida: $exit_code\n$errors-->";
		else if ($exit_code !== 0 && $exit_code !== 188) print "<p>Se devolvió un código de salida inesperado ($exit_code) sin mensajes de error.</p>\n";
	}
}
