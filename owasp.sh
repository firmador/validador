#!/bin/bash
# Usage: ./owasp.sh [-DnvdApiKey=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX]
mvn "$@" -U dependency-check:aggregate
