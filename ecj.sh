#!/bin/sh
# Eclipse Compiler for Java detects unused stuff and other checks beyond javac
mvn -DskipNativeBuild -DskipShade=false -Dmaven.compiler.compilerId=eclipse "$@" clean package
